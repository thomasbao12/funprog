module ListsAndTrees where

import List ((::))
import List

suffixes : List a -> List (List a)
suffixes xs = case xs of
                [] -> [[]]
                _ :: xs' -> xs :: suffixes xs'

type Tree = Empty | Node Int Tree Tree

mem : Int -> Tree -> Bool
mem x tree = case tree of
               Empty -> False
               Node y left right ->
                   if | x < y -> mem x left
                      | mem x right -> True
                      | otherwise -> x == y

                   {-x < y then mem x left
                   else if mem x right then True
                   else x == y-}

fullTree : Int -> Int -> Tree
fullTree x h =
    if | h == 0 -> Empty
       | otherwise -> let t = fullTree x (h-1) in
                      Node x t t

balancedTree : Int -> Int -> Tree
balancedTree x n = case n of
                     0 -> Empty
                     _ -> let l = (n-1)//2
                              r = (n-1-l)
                              ll = balancedTree x l
                              rr = if l == r then ll else balancedTree x r
                          in Node x ll rr

{-
    if | n == 0 -> Empty
       | n == 1 -> Node x Empty Empty
       | n%2 == 0 -> let t = balancedTree x (n//2) in Node x t t
       | n%2 == 1 -> let l = balancedTree x (n//2)
                         r = balancedTree x ((n//2)-1)
                     in Node x l r
-}

balanced : Tree -> Bool
balanced tree = case tree of
                    Empty -> True
                    Node x l r -> let numNodes t = case t of
                                                     Empty -> 0
                                                     Node y ll rr -> numNodes ll + numNodes rr + 1
                                  in abs (numNodes l - numNodes r) <= 1 &&
                                     balanced l && balanced r

-- did not use create2
create2 : Int -> Int -> Int -> (Tree, Tree)
create2 _ _ _ = (Empty, Empty)

balancedTrees : Int -> Int -> List Tree
balancedTrees x n =
    if | n == 0 -> [Empty]
       | n == 1 -> [Node x Empty Empty]
       | n%2 == 1 -> let t = balancedTree x (n//2) in (Node x t t) :: balancedTrees x (n//2 - 1)
       | n%2 == 0 -> let l = balancedTree x (n//2)
                         r = balancedTree x ((n//2)-1)
                     in (Node x l r) :: (Node x r l) :: (Node x r r) :: (Node x l l) :: balancedTrees x (n//2 - 1)

{- case n of
                      0 -> [Empty]
                      _ -> let l = (n-1)//2
                               r = (n-1-l)
                               ll = balancedTree x l
                               rr = if l == r then ll else balancedTree x r
                           in (Node x ll rr) :: balancedTrees x n
-}

--let t = balancedTrees x (n//2) in Node x t t

completeTrees : Int -> Int -> List Tree
completeTrees _ _ =
  -- TODO
  []

almostCompleteTrees : Int -> Int -> List Tree
almostCompleteTrees _ _ =
  -- TODO
  []

